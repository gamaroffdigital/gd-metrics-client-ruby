require 'httparty'
require 'json'

module GDMetrics
  class Client
    include HTTParty
    base_uri 'gdlnk.co/api'

    attr_reader :app_id, :app_secret

    def initialize(app_id, app_secret)
      @app_id     = app_id
      @app_secret = app_secret

      raise GDMetrics::Exception.new("An app_id and app_secret must be passed to GDMetrics::Client.new") if @app_id.to_s.empty? or @app_secret.to_s.empty?
    end

    def register_user(access_token, referrer_facebook_id = 0, custom_data = nil)
      params = {
        access_token: access_token,
        referrer_facebook_id: referrer_facebook_id,
        custom_data: custom_data.nil? ? {} : custom_data.to_json
      }

      handle_response self.class.get('/register', body: params.merge(auth_params))
    end

    def shorten_url(url, facebook_id = 0)
      params = {
        url: url,
        facebook_id: facebook_id
      }

      handle_response self.class.get('/shorten', body: params.merge(auth_params))
    end

    protected

    def auth_params
      { app_id: @app_id, app_secret: app_secret }
    end

    def handle_response(response)
      if response.code != 200
        raise GDMetrics::Exception.new("Unexpected response: #{response.body}")
      elsif response['error']
        raise GDMetrics::Exception.new("Error ##{response['error']['code']}: #{response['error']['message']}")
      else
        response.to_hash
      end
    end
  end
end