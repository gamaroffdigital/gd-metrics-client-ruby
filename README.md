# GD Metrics Ruby Client

Ahoy hoy. This allows you to easily implement GD Metrics in your Ruby app. Hurrah!

## Installation

Add this line to your application's Gemfile:

    gem 'gdmetrics', git: 'http://git.gamaroff.co/josh/gd-metrics-client-ruby.git'

And then bundle that puppy:

    $ bundle

## Usage

Firstly, instantiate the library:

    @client = GDMetrics::Client.new('YOUR_APP_ID', 'YOUR_APP_SECRET')

If you're using Rails and don't want to have to specify an app ID and secret each time you call the class, you can place an initializer in `config/initializers/gdmetrics.rb` which looks something like this:

    module GDMetrics
      APP_ID     = 'YOUR_APP_ID'
      APP_SECRET = 'YOUR_APP_SECRET'
    end

    GDMetrics::Client.class_eval do
      def initialize_with_default_settings(*args)
        case args.size
          when 0
            raise "no GDMetrics app_id or app_secret defined in initializer" unless GDMetrics::APP_ID and GDMetrics::APP_SECRET
            initialize_without_default_settings(GDMetrics::APP_ID.to_s, GDMetrics::APP_SECRET.to_s)
          when 1, 2
            initialize_without_default_settings(*args) 
        end
      end 

      alias_method_chain :initialize, :default_settings 
    end

This allows you to shorten that instantiation to something like this:

    @client = GDMetrics::Client.new

From there, you've got a couple of useful methods at your disposal:

### register_user(access_token, referrer_facebook_id = 0, custom_data = nil)

This registers users for tracking.

#### Parameters:

- **access_token** - a valid Facebook access token obtained via the JS/PHP SDK or some other means
- **referrer_facebook_id** - the referring user's Facebook ID optional *(optional)*
- **custom_data** - a Hash containing custom data about the user relevant to the app *(optional)*

Response on success is a hash which looks a bit like `{"success" => true}`. Raises `GDMetrics::Exception` on error.

#### Example usage:

    begin
      @client = GDMetrics::Client.new
      @client.register_user(access_token, '600859899', { favourite_color: 'purple' })
    rescue GDMetrics::Exception
      puts 'oh no!'
    end

### shorten_url(url, facebook_id = 0)

This shortens long URLs and provides you with a slug to create a full gdlnk.co URL.

#### Parameters:

- **url** - the URL you want to shorten
- **facebook_id** - the Facebook ID of the user generating the short link

Response on success is a hash which looks a bit like `{"link" => {"url" => "http://example.com", "slug" => "xFaC"}}`. The `slug` key can be used to form a gdlnk.co URL - e.g. gdlnk.co/xFaC.

Raises `GDMetrics::Exception` on error.

#### Example usage:

    begin
      @client = GDMetrics::Client.new
      @client.shorten_url('http://example.com', '600859899')
    rescue GDMetrics::Exception
      puts 'oh no!'
    end

## Testing

Unit testing is provided courtesy of RSpec. To run them, just run `rspec` at the command line.