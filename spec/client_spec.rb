require 'spec_helper'

describe GDMetrics::Client do
  before :all do
    credentials = YAML::load(File.read('fixtures/credentials.yml'))

    @app_id     = credentials['app_id']
    @app_secret = credentials['app_secret']
    @client     = GDMetrics::Client.new(@app_id, @app_secret)
  end

  describe '#new' do
    it 'takes two parameters and returns a GDMetrics::Client object' do
      @client.should be_an_instance_of GDMetrics::Client
    end

    it 'sets a readable app_id attribute' do
      @client.app_id.should eq @app_id
    end

    it 'sets a readable app_secret attribute' do
      @client.app_secret.should eq @app_secret
    end

    it 'raises an exception if empty app_id or app_secret are supplied' do
      expect {
        GDMetrics::Client.new('', '')
      }.to raise_exception GDMetrics::Exception
    end
  end
  
  describe '.register_user' do
    it 'raises an exception if using an invalid app_id/secret' do
      VCR.use_cassette('register_user_with_invalid_app_id_secret') do
        expect {
          GDMetrics::Client.new('A_FAKE_APP_ID', 'A_FAKE_APP_SECRET').register_user('A_FAKE_USER')
        }.to raise_exception GDMetrics::Exception
      end
    end

    it 'raises an exception if passed custom data which is not convertable to a JSON dictionary' do
      VCR.use_cassette('register_user_with_invalid_custom_data') do
        expect {
          puts @client.register_user('abc', 0, 'SOME_FAKE_DATA')
        }.to raise_exception GDMetrics::Exception
      end
    end

    it 'returns a hash containing one item, "success", with a value of true' do
      VCR.use_cassette('register_user_successfully') do
        response = @client.register_user('abc')
        response.should be_a Hash
        response.length.should eq 1
        response['success'].should be_true
      end
    end
  end

  describe '.shorten_url' do
    it 'raises an exception if using an invalid app_id/secret' do
      VCR.use_cassette('shorten_url_with_invalid_app_id_secret') do
        expect {
          GDMetrics::Client.new('A_FAKE_APP_ID', 'A_FAKE_APP_SECRET').shorten_url('A_FAKE_URL')
        }.to raise_exception GDMetrics::Exception
      end
    end

    it 'raises an exception if passed an invalid URL' do
      VCR.use_cassette('shorten_url_with_invalid_url') do
        expect {
          @client.shorten_url('A_FAKE_URL')
        }.to raise_exception GDMetrics::Exception
      end
    end

    it 'raises an exception if passed a Facebook ID not registered with this application' do
      VCR.use_cassette('shorten_url_with_invalid_facebook_id') do
        expect {
          @client.shorten_url('http://example.com', -1)
        }.to raise_exception GDMetrics::Exception
      end
    end

    it 'returns a hash of URL data' do
      VCR.use_cassette('shorten_url_successfully') do
        response = @client.shorten_url('http://example.com')
        response.should be_a Hash
        response['link'].should be_a Hash
      end
    end
  end
end