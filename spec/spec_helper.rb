Dir.chdir File.dirname(__FILE__)

require 'gdmetrics'
require 'yaml'
require 'vcr'

VCR.configure do |c|
  c.cassette_library_dir = 'fixtures/vcr_cassettes'
  c.hook_into :webmock
end
